package br.com.ldrson.oop.enums;

/**
 * Created by leanderson on 13/06/16.
 */
public enum Direcao {

    ESQUERDA(0), DIREITA(1), CIMA(2), BAIXO(3);

    private int valor;

    private Direcao(int valor){
        this.valor = valor;
    }

    public int obterValor(){
        return valor;
    }

}
