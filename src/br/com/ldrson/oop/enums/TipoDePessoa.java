package br.com.ldrson.oop.enums;

/**
 * Created by leanderson on 13/06/16.
 */
public enum TipoDePessoa {

    FISICA, JURIDICA;

}
