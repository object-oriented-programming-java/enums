package br.com.ldrson.oop.enums;

/**
 * Created by leanderson on 13/06/16.
 */
public enum Cor {

    AMARELO("FFFF00"), VERDE("339900"), AZUL("0000CC"), VERMELHO("FF0000"), PRETO("ffffff");

    private String valor;

    private Cor(String valor){
        this.valor = valor;
    }

    public String obterValorEmHexadecimal(){
        return valor;
    }

}
